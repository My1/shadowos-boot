#!ipxe

set menu-timeout 10000
isset ${menu-default} || set menu-default boot_stable
console --x 1000 --y 625
console --picture ${boot-url}/shadow_bg.png --left 30 --right 30 --top 30 --bottom 30
cpair --foreground 7 --background 15 2
cpair --foreground 0 --background 15 3

<?php if (isset($_GET['arch']) && $_GET['arch'] == "i386"):
  echo "echo BOOT : Hardware unsupported. This software is only compatible with 64 bit components.\n";
  echo "goto failed";
endif; ?>

:menu
  menu ShadowOS Boot Menu

  item --gap -- Choose ShadowOS version
  item --key 0 boot_stable (0) Boot from network on stable version of ShadowOS
  item --key 1 boot_stable_dis_nv (1) Boot from network on stable version of ShadowOS (Nouveau Disabled)
  item --key 2 boot_unstable (2) Boot from network on testing version of ShadowOS
  item --key 3 boot_unstable_dis_nv (3) Boot from network on testing version of ShadowOS (Nouveau Disabled)
  item --key 4 boot_test (4) Test Boot
  item --gap -- Test tools
  item --key 5 test_hardware (5) Hardware (Not Working)
  item --key 6 test_memtest (6) Memory (Memtest)
  item --key 7 test_gputest (7) Gpu (Not Working)
  item --gap -- Advanced options
  item --key s shell (S)  Shell
  item --key r reboot (R)  Reboot computer
  item --key x shutdown (X) Shutdown
  choose --timeout ${menu-timeout} --default ${menu-default} selected || goto cancel
  set menu-timeout 0
  goto ${selected}

:boot_stable
  echo BOOT : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live/ quiet splash
  initrd http://91.121.38.1:8080/live/arch/boot/x86_64/archiso.img
  boot

:boot_stable_dis_nv
  echo BOOT : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live/ quiet splash splash nouveau.modeset=0
  initrd http://91.121.38.1:8080/live/arch/boot/x86_64/archiso.img
  boot

:boot_unstable
  echo BOOT : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live_testing/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live_testing/ quiet splash
  initrd http://91.121.38.1:8080/live_testing/arch/boot/x86_64/archiso.img
  boot

:boot_unstable_dis_nv
  echo BOOT : retrieving boot files (Please wait...)
  kernel http://91.121.38.1:8080/live_testing/arch/boot/x86_64/vmlinuz initrd=archiso.img archisobasedir=arch ip=dhcp net.ifnames=0 archiso_http_srv=http://91.121.38.1:8080/live_testing/ quiet splash nouveau.modeset=0
  initrd http://91.121.38.1:8080/live_testing/arch/boot/x86_64/archiso.img
  boot

:test_hardware
  echo Starting hardware test
  goto shell

:test_memtest
  echo Starting memory test
  sanboot http://91.121.38.1:8080/live/Memtest86-7.5.iso || goto failed

:test_gputest
  echo Starting gpu test
  goto shell

:reboot
  reboot

:shutdown
  poweroff

:failed
  echo Booting failed, dropping to shell and freeing memory.
  imgfree
  goto shell

:shell
  echo Type 'poweroff' to shutdown the device.
  echo Type 'reboot' to reboot the device.
  echo Type 'help' for more IPXE commands.
  echo .......................................
  route #IP INFO
  echo
  shell

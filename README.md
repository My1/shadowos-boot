# Live OS install

![Pipeline state](https://gitlab.com/aar642/shadowos-boot/badges/master/build.svg)

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Known Issues](#known-issues)
- [Windows USB flash drive Install](#windows-usb-flash-drive-install)
	- [Requirements](#requirements)
	- [USB creation](#usb-creation)
		- [Hybrid Boot](#hybrid-boot)
- [Linux USB flash drive Install](#linux-usb-flash-drive-install)
	- [Requirements](#requirements)
	- [easy2boot installation](#easy2boot-installation)
	- [easy2boot usage](#easy2boot-usage)
- [Boot Shadow Live](#boot-shadow-live)
- [HTTP boot](#http-boot)
	- [Requirements](#requirements)
- [Boot Shadow Live](#boot-shadow-live)
- [FAQ](#faq)
- [Discord servers](#discord-servers)
- [Support](#support)
- [Maintainers](#maintainers)
- [Disclaimer](#disclaimer)

<!-- /TOC -->

## Introduction

This project will network boot in RAM you into a fully fonctionnal Linux OS with Shadow, at first it was aimed to give a second life to old hardware with broken hard drive but quickly it was also nice for many others use-cases. (Debug, Easy Check for Linux compliance, Work PC, Easy switch from Windows to Linux...)

Booting into this Live OS to use Shadow requires no Linux knowledge at all.

**So far we handle:**
 - UK/US/DE/FR (+Apple Variant) Keyboard
 - USB Mouse + TouchePads
 - Sound Output
 - Sound Input (Microphones)
 - Gamepads (wired)

## Requirements

- **4096MB+** of RAM x86_64 PC with an **ethernet RJ45 cable** and served by **DHCP server** (Shadow Stream won't start on a Virtual Machine, it requires a GPU with hardware decoding feature)
- **Compatible GPU** Intel/AMD/"Nvidia(**> Geforce 8** & **< GTX 750**), it should work automaticaly if you have an **Intel iGPU available**" with dedicated hardware H.26x decoder
- Fast connection or enough time to download approximatively **1GB** (If you use an USB/Ethernet adapter, please plug it on an USB 2 port if it's not working)
- Enough knowledge to flash an USB flash drive and boot on it from BIOS
- The **Secure Boot** option must be disabled in the bios to be able to boot on the USB drive on **EFI bios**

## Known Issues

- It's **required** to start Shadow in **fullscreen** or it **won't** work !
- AZERTY keyboard layout by default (you can change that with a right click on the desktop, open an issue if you want additional keyboard layout)
- Some network card can't be find in EFI boot mode. Try to change your boot configuration to legacy to fix the problem
- Some Nvidia GPU hangs during boot (use at boot "Nouveau disabled" entries)
- **NVIDIA GPU**: > Geforce 8 & < GTX 750 is still a work in progress, very old one and newer are unsupported
- WiFi/Bluetooth is not yet supported once booted in ShadowOS
- Shadow Credentials are **not saved** (it is planned to be saved on USB Flash Drive at some point)
- Shadow Authentication code **is requested after each reboot**

> *If you find an issue, feel free to report it here: https://gitlab.com/aar642/shadowos-boot/issues*

## Windows USB flash drive Install

### Requirements

- 32MB+ USB Flash Drive
- Download the file here for Hybrid Boot (Legacy and EFI): https://gitlab.com/aar642/shadowos-boot/-/jobs/118212364/artifacts/raw/ShadowBoot-hybrid_1.4.iso
- Download Rufus (http://rufus.ie): https://github.com/pbatard/rufus/releases/download/v3.3/rufus-3.3.exe

### USB creation

- Connect your USB key to your computer
- Launch Rufus
- Follow the screenshots below
- Set volume's label to : ShadowOS
- **This steps will wipe all the datas on the USB key**

![Rufus Warning](Docs/warning.PNG "Rufus Warning")

#### Hybrid Boot

![Hybrid Boot](Docs/hybrid_1.PNG "Rufus setup for Hybrid Boot")

**You may have a warning message. Just click OK.**

![Legacy Boot warning](Docs/il_warning.PNG "Rufus Warning Syslinux")

## Linux USB flash drive Install

### Requirements

- 32MB+ USB Flash Drive
- Download the file here for Hybrid Boot (Legacy and EFI): https://gitlab.com/aar642/shadowos-boot/-/jobs/118212364/artifacts/raw/ShadowBoot-hybrid_1.4.iso
- Download and install easy2boot

### easy2boot installation

```
# Go to your home directory
cd ~/

# Download easy2boot and unzip it
wget http://files.easy2boot.com/200003320-721c073154/Easy2Boot_v1.A6.zip  && unzip Easy2Boot*.zip -d Easy2Boot/

# Go to the scripts folder and make them executable
cd Easy2Boot/_ISO/docs/linux_utils && chmod u+x *
```

### easy2boot usage

**This step will wipe all the datas on the device. Be sure to select the good device**

```
# Go to the easy2boot installation folder
cd ~/Easy2Boot/_ISO/docs/linux_utils

# Launch the ftm.sh script and follow the steps. Be sure to select the good device to format.
sudo bash ./fmt.sh

# Now, you need to put the ISO on the folder _ISO/ on the device. And you can enjoy a beautiful future with Shadow OS Live !

```

## Boot Shadow Live
- Change Bios to boot on USB Flash Drive
- Wait for timer or select "Boot from network stable version of ShadowOS" and press enter
- Wait until Shadow Live is fully booted
- **Connect to your Shadow  !**

## HTTP boot

### Requirements

 - An UEFI Bios with HTTP Boot option

## Boot Shadow Live

 - Enter URL in Bios HTTP Boot option : http://91.121.38.1:8080/shadow.efi
 - Boot from HTTP Boot option
 - **Connect to your Shadow !**

# Boot Menu

![Boot Menu](Docs/boot_menu.png "Boot Menu")

![ShadowOS](Docs/shadow_os.png "Shadow OS")

# FAQ, Communities and Maintainers

## FAQ

1. Why not provide an ISO and why networked boot ?

  We may provide at some point directly an ISO, but we thought that networked boot allows us to push updates/fixes more quickly and it's the future !

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)

## Support

| Success on             | Failed on      |
| :--------------------: | :------------: |
| Nvidia GTX 760         | Nvidia 750 TI  | 
| Nvidia GT 720M         | Intel GMA      | 
| Nvidia GTX 680         |                |      
| Nvidia 550 TI          |                | 
| AMD Radeon 520         |                | 
| Intel HD Graphics 4400 |                | 

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![GiantPandaRoux#0777](https://cdn.discordapp.com/avatars/267044035032514561/e98147b99f4821c4e806e97fda05e69a.png?size=64 "GiantPandaRoux#0777")
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
